/* OGMJob - A library to spawn processes
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __OGMJOB_H__
#define __OGMJOB_H__

#include <ogmjob-bin.h>
#include <ogmjob-container.h>
#include <ogmjob-exec.h>
#include <ogmjob-list.h>
#include <ogmjob-log.h>
#include <ogmjob-pipeline.h>
#include <ogmjob-queue.h>
#include <ogmjob-spawn.h>

#endif /* __OGMJOB_H__ */

