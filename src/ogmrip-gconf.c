/* OGMRip - A DVD Encoder for GNOME
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "ogmrip-fs.h"
#include "ogmrip-gconf.h"
#include "ogmrip-helper.h"
#include "ogmrip-plugin.h"

#include "ogmrip-gconf-settings.h"

#include <string.h>
#include <unistd.h>

OGMRipSettings *settings;

GType
ogmrip_gconf_get_container_type (const gchar *section, const gchar *name)
{
  GType container;
  gchar *str;

  if (name)
    str = g_strdup (name);
  else
    ogmrip_settings_get (settings, section, OGMRIP_GCONF_CONTAINER_FORMAT, &str, NULL);

  container = ogmrip_plugin_get_container_by_name (str);
  g_free (str);

  return container;
}

GType
ogmrip_gconf_get_video_codec_type (const gchar *section, const gchar *name)
{
  GType codec;
  gchar *str;

  if (name)
    str = g_strdup (name);
  else
    ogmrip_settings_get (settings, section, OGMRIP_GCONF_VIDEO_CODEC, &str, NULL);

  codec = ogmrip_plugin_get_video_codec_by_name (str);
  g_free (str);

  return codec;
}

GType
ogmrip_gconf_get_audio_codec_type (const gchar *section, const gchar *name)
{
  GType codec;
  gchar *str;

  if (name)
    str = g_strdup (name);
  else
    ogmrip_settings_get (settings, section, OGMRIP_GCONF_AUDIO_CODEC, &str, NULL);

  codec = ogmrip_plugin_get_audio_codec_by_name (str);
  g_free (str);

  return codec;
}

GType
ogmrip_gconf_get_subp_codec_type (const gchar *section, const gchar *name)
{
  GType codec;
  gchar *str;

  if (name)
    str = g_strdup (name);
  else
    ogmrip_settings_get (settings, section, OGMRIP_GCONF_SUBP_CODEC, &str, NULL);

  codec = ogmrip_plugin_get_subp_codec_by_name (str);
  g_free (str);

  return codec;
}

void
ogmrip_gconf_init (void)
{
  settings = ogmrip_gconf_settings_new (OGMRIP_GCONF_ROOT);
  ogmrip_settings_set_default (settings);

  /*
   * Preferences
   */

  ogmrip_settings_install_key (settings,
      g_param_spec_string (OGMRIP_GCONF_PROFILE, NULL, NULL, OGMRIP_DEFAULT_PROFILE, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_string (OGMRIP_GCONF_OUTPUT_DIR, NULL, NULL, OGMRIP_DEFAULT_OUTPUT_DIR, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_string (OGMRIP_GCONF_TMP_DIR, NULL, NULL, OGMRIP_DEFAULT_TMP_DIR, G_PARAM_READWRITE));

  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_FILENAME, NULL, NULL, 0, 3, OGMRIP_DEFAULT_FILENAME, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_PREF_AUDIO, NULL, NULL, 0, G_MAXINT, OGMRIP_DEFAULT_PREF_AUDIO, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_PREF_SUBP, NULL, NULL, 0, G_MAXINT, OGMRIP_DEFAULT_PREF_SUBP, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_CHAPTER_LANG, NULL, NULL, 0, G_MAXINT, OGMRIP_DEFAULT_CHAPTER_LANG, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_AFTER_ENC, NULL, NULL, 0, 3, OGMRIP_DEFAULT_AFTER_ENC, G_PARAM_READWRITE));

#ifdef HAVE_SYSCONF_NPROC
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_THREADS, NULL, NULL, 1, G_MAXINT, sysconf (_SC_NPROCESSORS_ONLN), G_PARAM_READWRITE));
#else
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_THREADS, NULL, NULL, 1, G_MAXINT, OGMRIP_DEFAULT_THREADS, G_PARAM_READWRITE));
#endif

  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_COPY_DVD, NULL, NULL, OGMRIP_DEFAULT_COPY_DVD, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_KEEP_TMP, NULL, NULL, OGMRIP_DEFAULT_KEEP_TMP, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_LOG_OUTPUT, NULL, NULL, OGMRIP_DEFAULT_LOG_OUTPUT, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_AUTO_SUBP, NULL, NULL, OGMRIP_DEFAULT_AUTO_SUBP, G_PARAM_READWRITE));

  ogmrip_settings_install_key (settings,
      g_param_spec_string (OGMRIP_GCONF_PROFILE_NAME, NULL, NULL, NULL, G_PARAM_READWRITE));

  /*
   * Container
   */

  ogmrip_settings_install_key (settings,
      g_param_spec_string (OGMRIP_GCONF_CONTAINER_FORMAT, NULL, NULL, OGMRIP_DEFAULT_CONTAINER_FORMAT, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_CONTAINER_FOURCC, NULL, NULL, 0, 4, OGMRIP_DEFAULT_CONTAINER_FOURCC, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_CONTAINER_TNUMBER, NULL, NULL, 1, G_MAXINT, OGMRIP_DEFAULT_CONTAINER_TNUMBER, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_CONTAINER_TSIZE, NULL, NULL, 1, G_MAXINT, OGMRIP_DEFAULT_CONTAINER_TSIZE, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_CONTAINER_ENSURE_SYNC, NULL, NULL, OGMRIP_DEFAULT_CONTAINER_ENSURE_SYNC, G_PARAM_READWRITE));

  /*
   * Video
   */

  ogmrip_settings_install_key (settings,
      g_param_spec_string (OGMRIP_GCONF_VIDEO_CODEC, NULL, NULL, OGMRIP_DEFAULT_VIDEO_CODEC, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_PASSES, NULL, NULL, 1, G_MAXINT, OGMRIP_DEFAULT_VIDEO_PASSES, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_PRESET, NULL, NULL, 0, 3, OGMRIP_DEFAULT_VIDEO_PRESET, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_SCALER, NULL, NULL, 0, 10, OGMRIP_DEFAULT_VIDEO_SCALER, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_ASPECT, NULL, NULL, 0, 2, OGMRIP_DEFAULT_VIDEO_ASPECT, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_VIDEO_DENOISE, NULL, NULL, OGMRIP_DEFAULT_VIDEO_DENOISE, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_VIDEO_TRELLIS, NULL, NULL, OGMRIP_DEFAULT_VIDEO_TRELLIS, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_VIDEO_QPEL, NULL, NULL, OGMRIP_DEFAULT_VIDEO_QPEL, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_VIDEO_DEBLOCK, NULL, NULL, OGMRIP_DEFAULT_VIDEO_DEBLOCK, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_VIDEO_DERING, NULL, NULL, OGMRIP_DEFAULT_VIDEO_DERING, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_VIDEO_TURBO, NULL, NULL, OGMRIP_DEFAULT_VIDEO_TURBO, G_PARAM_READWRITE));

  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_ENCODING, NULL, NULL, 0, 2, OGMRIP_DEFAULT_VIDEO_ENCODING, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_BITRATE, NULL, NULL, 4, 16000, OGMRIP_DEFAULT_VIDEO_BITRATE, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_double (OGMRIP_GCONF_VIDEO_QUANTIZER, NULL, NULL, 0, 31, OGMRIP_DEFAULT_VIDEO_QUANTIZER, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_double (OGMRIP_GCONF_VIDEO_BPP, NULL, NULL, 0.0, G_MAXDOUBLE, OGMRIP_DEFAULT_VIDEO_BPP, G_PARAM_READWRITE));

  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_VIDEO_CAN_CROP, NULL, NULL, OGMRIP_DEFAULT_VIDEO_CAN_CROP, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_VIDEO_CAN_SCALE, NULL, NULL, OGMRIP_DEFAULT_VIDEO_CAN_SCALE, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_VIDEO_EXPAND, NULL, NULL, OGMRIP_DEFAULT_VIDEO_EXPAND, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_MAX_WIDTH, NULL, NULL, 0, G_MAXINT, OGMRIP_DEFAULT_VIDEO_MAX_WIDTH, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_MAX_HEIGHT, NULL, NULL, 0, G_MAXINT, OGMRIP_DEFAULT_VIDEO_MAX_HEIGHT, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_MIN_WIDTH, NULL, NULL, 0, G_MAXINT, OGMRIP_DEFAULT_VIDEO_MIN_WIDTH, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_VIDEO_MIN_HEIGHT, NULL, NULL, 0, G_MAXINT, OGMRIP_DEFAULT_VIDEO_MIN_HEIGHT, G_PARAM_READWRITE));

  /*
   * Audio
   */

  ogmrip_settings_install_key (settings,
      g_param_spec_string (OGMRIP_GCONF_AUDIO_CODEC, NULL, NULL, OGMRIP_DEFAULT_AUDIO_CODEC, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_AUDIO_QUALITY, NULL, NULL, 0, 10, OGMRIP_DEFAULT_AUDIO_QUALITY, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_AUDIO_CHANNELS, NULL, NULL, 0, 3, OGMRIP_DEFAULT_AUDIO_CHANNELS, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_AUDIO_SRATE, NULL, NULL, 0, 9, OGMRIP_DEFAULT_AUDIO_SRATE, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_AUDIO_NORMALIZE, NULL, NULL, OGMRIP_DEFAULT_AUDIO_NORMALIZE, G_PARAM_READWRITE));

  /*
   * Subtitles
   */

  ogmrip_settings_install_key (settings,
      g_param_spec_string (OGMRIP_GCONF_SUBP_CODEC, NULL, NULL, OGMRIP_DEFAULT_SUBP_CODEC, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_SUBP_CHARSET, NULL, NULL, 0, 2, OGMRIP_DEFAULT_SUBP_CHARSET, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_int (OGMRIP_GCONF_SUBP_NEWLINE, NULL, NULL, 0, 1, OGMRIP_DEFAULT_SUBP_NEWLINE, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_FORCED_SUBS, NULL, NULL, OGMRIP_DEFAULT_FORCED_SUBS, G_PARAM_READWRITE));
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_SPELL_CHECK, NULL, NULL, OGMRIP_DEFAULT_SPELL_CHECK, G_PARAM_READWRITE));
/*
  ogmrip_settings_install_key (settings,
      g_param_spec_boolean (OGMRIP_GCONF_SUBP_HARDCODE, NULL, NULL, OGMRIP_DEFAULT_SUBP_HARDCODE, G_PARAM_READWRITE));
*/
}

void
ogmrip_gconf_uninit (void)
{
  if (settings)
  {
    ogmrip_settings_set_default (NULL);
    g_object_unref (settings);
    settings = NULL;
  }
}

