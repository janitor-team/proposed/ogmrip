
#ifndef __ogmrip_cclosure_marshal_MARSHAL_H__
#define __ogmrip_cclosure_marshal_MARSHAL_H__

#include	<glib-object.h>

G_BEGIN_DECLS

/* VOID:STRING,STRING (ogmrip-marshal.list:24) */
extern void ogmrip_cclosure_marshal_VOID__STRING_STRING (GClosure     *closure,
                                                         GValue       *return_value,
                                                         guint         n_param_values,
                                                         const GValue *param_values,
                                                         gpointer      invocation_hint,
                                                         gpointer      marshal_data);

G_END_DECLS

#endif /* __ogmrip_cclosure_marshal_MARSHAL_H__ */

