/* OGMRip - A library for DVD ripping and encoding
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __OGMRIP_SETTINGS_H__
#define __OGMRIP_SETTINGS_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define OGMRIP_TYPE_SETTINGS            (ogmrip_settings_get_type ())
#define OGMRIP_SETTINGS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), OGMRIP_TYPE_SETTINGS, OGMRipSettings))
#define OGMRIP_IS_SETTINGS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), OGMRIP_TYPE_SETTINGS))
#define OGMRIP_SETTINGS_GET_IFACE(inst) (G_TYPE_INSTANCE_GET_INTERFACE ((inst), OGMRIP_TYPE_SETTINGS, OGMRipSettingsIface))

typedef struct _OGMRipSettings      OGMRipSettings;
typedef struct _OGMRipSettingsIface OGMRipSettingsIface;

/**
 * OGMRipNotifyFunc:
 * @settings: An #OGMRipSettings
 * @section: A section
 * @key: A key
 * @value: A #GValue
 * @data: The user data
 *
 * Specifies the type of functions passed to ogmrip_settings_add_notify(),
 * and ogmrip_settings_add_notify_while_alive().
 */
typedef void (* OGMRipNotifyFunc) (OGMRipSettings *settings,
                                   const gchar    *section,
                                   const gchar    *key,
                                   const GValue   *value,
                                   gpointer       data);

/**
 * OGMRipGetFunc:
 * @object: A #GObject
 * @property: A property
 * @value: A #GValue
 * @data: The user data
 *
 * Specifies the type of functions passed to ogmrip_settings_bind_custom() to get
 * the value of the property.
 */
typedef void (* OGMRipGetFunc) (GObject      *object,
                                const gchar  *property,
                                GValue       *value,
                                gpointer     data);

/**
 * OGMRipSetFunc:
 * @object: A #GObject
 * @property: A property
 * @value: A #GValue
 * @data: The user data
 *
 * Specifies the type of functions passed to ogmrip_settings_bind_custom() to set
 * the value of the property.
 */
typedef void (* OGMRipSetFunc) (GObject      *object,
                                const gchar  *property,
                                const GValue *value,
                                gpointer     data);

/**
 * OGMRipParseFunc:
 * @node: An #xmlNode
 * @user_data: The user data
 *
 * Specifies the type of the function passed to ogmrip_settings_parse for each
 * node in an XML file.
 *
 * Returns: %FALSE to stop calling the function.
 */
typedef gboolean (* OGMRipParseFunc) (gpointer node,
                                      gpointer user_data);

struct _OGMRipSettingsIface
{
  GTypeInterface base_iface;

  /*
   * Methods
   */

  void             (* install_key)      (OGMRipSettings   *settings,
                                         GParamSpec       *pspec);

  GType            (* get_type)         (OGMRipSettings   *settings,
                                         const gchar      *section,
                                         const gchar      *key);

  void             (* set_value)        (OGMRipSettings   *settings,
                                         const gchar      *section,
                                         const gchar      *key,
                                         const GValue     *value);
  void             (* get_value)        (OGMRipSettings   *settings,
                                         const gchar      *section,
                                         const gchar      *key,
                                         GValue           *value);

  void             (* sync)             (OGMRipSettings   *settings);

  gchar *          (* build_section)    (OGMRipSettings   *settings,
                                         const gchar      *element,
                                         va_list          var_args);
  const gchar *    (* get_section_name) (OGMRipSettings   *settings,
                                         const gchar      *section);

  GSList *         (* get_subsections)  (OGMRipSettings   *settings,
                                         const gchar      *section);
  GSList *         (* get_keys)         (OGMRipSettings   *settings,
                                         const gchar      *section,
                                         gboolean         recursive);

  void             (* remove_key)       (OGMRipSettings   *settings,
                                         const gchar      *section,
                                         const gchar      *key);
  void             (* remove_section)   (OGMRipSettings   *settings,
                                         const gchar      *section);

  gboolean         (* has_key)          (OGMRipSettings   *settings,
                                         const gchar      *section,
                                         const gchar      *key);
  gboolean         (* has_section)      (OGMRipSettings   *settings,
                                         const gchar      *section);

  gulong           (* add_notify)       (OGMRipSettings   *settings, 
                                         const gchar      *section,
                                         const gchar      *key,
                                         OGMRipNotifyFunc func,
                                         gpointer         data);
  void             (* remove_notify)    (OGMRipSettings   *setgings, 
                                         gulong           handler_id);
};

GType            ogmrip_settings_get_type                  (void) G_GNUC_CONST;

OGMRipSettings * ogmrip_settings_get_default               (void);
void             ogmrip_settings_set_default               (OGMRipSettings   *settings);

void             ogmrip_settings_install_key               (OGMRipSettings   *settings,
                                                            GParamSpec       *pspec);

GParamSpec *     ogmrip_settings_find_key                  (OGMRipSettings   *settings,
                                                            const gchar      *key);

GType            ogmrip_settings_get_key_type              (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key);

void             ogmrip_settings_get_value                 (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key,
                                                            GValue           *value); 
void             ogmrip_settings_set_value                 (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key,
                                                            const GValue     *value);

void             ogmrip_settings_get                       (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key,
                                                            ...);
void             ogmrip_settings_set                       (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key,
                                                            ...);

void             ogmrip_settings_sync                      (OGMRipSettings   *settings);

gchar *          ogmrip_settings_build_section             (OGMRipSettings   *settings,
                                                            const gchar      *element,
                                                            ...);
const gchar *    ogmrip_settings_get_section_name          (OGMRipSettings   *settings,
                                                            const gchar      *section);

GSList *         ogmrip_settings_get_subsections           (OGMRipSettings   *settings,
                                                            const gchar      *section);
GSList *         ogmrip_settings_get_keys                  (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            gboolean         recursive);

void             ogmrip_settings_remove_key                (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key);
void             ogmrip_settings_remove_section            (OGMRipSettings   *settings,
                                                            const gchar      *section);

gboolean         ogmrip_settings_has_key                   (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key);
gboolean         ogmrip_settings_has_section               (OGMRipSettings   *settings,
                                                            const gchar      *section);

gulong           ogmrip_settings_add_notify                (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key,
                                                            OGMRipNotifyFunc func,
                                                            gpointer         data);
gulong           ogmrip_settings_add_notify_while_alive    (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key,
                                                            OGMRipNotifyFunc func,
                                                            gpointer         data,
                                                            GObject          *object);
void             ogmrip_settings_remove_notify             (OGMRipSettings   *settings,
                                                            gulong           handler_id);

void             ogmrip_settings_bind                      (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key,
                                                            GObject          *object,
                                                            const gchar      *property);
void             ogmrip_settings_bind_custom               (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key,
                                                            GObject          *object,
                                                            const gchar      *property,
                                                            OGMRipGetFunc    get_func,
                                                            OGMRipSetFunc    set_func,
                                                            gpointer         data);
void             ogmrip_settings_unbind                    (OGMRipSettings   *settings,
                                                            GObject          *object);

void             ogmrip_settings_block                     (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key);
void             ogmrip_settings_unblock                   (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *key);

gboolean         ogmrip_settings_export                    (OGMRipSettings   *settings,
                                                            const gchar      *section,
                                                            const gchar      *filename,
                                                            GError           **error);
gboolean         ogmrip_settings_import                    (OGMRipSettings   *settings,
                                                            const gchar      *filename,
                                                            gchar            **section,
                                                            GError           **error);
gboolean         ogmrip_settings_parse                     (OGMRipSettings   *settings,
                                                            const gchar      *filename,
                                                            OGMRipParseFunc  func,
                                                            gpointer         user_data,
                                                            GError           **error);

void             ogmrip_settings_install_key_from_property (OGMRipSettings   *settings,
                                                            GObjectClass     *klass,
                                                            const gchar      *section,
                                                            const gchar      *key,
                                                            const gchar      *property);
void             ogmrip_settings_set_property_from_key     (OGMRipSettings   *settings,
                                                            GObject          *object,
                                                            const gchar      *property,
                                                            const gchar      *section,
                                                            const gchar      *key);

gint             ogmrip_settings_compare_versions          (const gchar      *version1,
                                                            const gchar      *version2);
G_END_DECLS

#endif /* __OGMRIP_SETTINGS_H__ */

