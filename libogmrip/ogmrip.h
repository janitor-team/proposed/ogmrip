/* OGMRip - A library for DVD ripping and encoding
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __OGMRIP_H__
#define __OGMRIP_H__

#include <ogmrip-audio-codec.h>
#include <ogmrip-chapters.h>
#include <ogmrip-container.h>
#include <ogmrip-dvdcpy.h>
#include <ogmrip-edl.h>
#include <ogmrip-encoding.h>
#include <ogmrip-encoding-manager.h>
#include <ogmrip-enums.h>
#include <ogmrip-file.h>
#include <ogmrip-fs.h>
#include <ogmrip-hardsub.h>
#include <ogmrip-keyfile-settings.h>
#include <ogmrip-player.h>
#include <ogmrip-plugin.h>
#include <ogmrip-subp-codec.h>
#include <ogmrip-version.h>
#include <ogmrip-video-codec.h>

#endif /* __OGMRIP_H__ */
