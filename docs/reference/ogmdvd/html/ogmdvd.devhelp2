<?xml version="1.0" encoding="utf-8" standalone="no"?>
<!DOCTYPE book PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "">
<book xmlns="http://www.devhelp.net/book" title="OGMDvd Reference Manual" link="index.html" author="" name="ogmdvd" version="2" language="c">
  <chapters>
    <sub name="Types and functions" link="ogmdvd-ref.html">
      <sub name="Types" link="ogmdvd-Types.html"/>
      <sub name="Enumerations" link="ogmdvd-Enumerations.html"/>
      <sub name="Conversion functions" link="ogmdvd-Conversion-functions.html"/>
      <sub name="Reader" link="ogmdvd-Reader.html"/>
      <sub name="Parser" link="ogmdvd-Parser.html"/>
    </sub>
    <sub name="Library Reference" link="ogmdvd-api.html">
      <sub name="OGMDvdDisc" link="ogmdvd-OGMDvdDisc.html"/>
      <sub name="OGMDvdTitle" link="ogmdvd-OGMDvdTitle.html"/>
      <sub name="OGMDvdStream" link="ogmdvd-OGMDvdStream.html"/>
      <sub name="OGMDvdAudio" link="ogmdvd-OGMDvdAudio.html"/>
      <sub name="OGMDvdSubp" link="ogmdvd-OGMDvdSubp.html"/>
      <sub name="OGMDvdMonitor" link="ogmdvd-OGMDvdMonitor.html"/>
      <sub name="OGMDvdDrive" link="ogmdvd-OGMDvdDrive.html"/>
    </sub>
  </chapters>
  <functions>
    <keyword type="struct" name="OGMDvdDisc" link="ogmdvd-Types.html#OGMDvdDisc"/>
    <keyword type="struct" name="OGMDvdTitle" link="ogmdvd-Types.html#OGMDvdTitle"/>
    <keyword type="struct" name="OGMDvdStream" link="ogmdvd-Types.html#OGMDvdStream"/>
    <keyword type="struct" name="OGMDvdAudioStream" link="ogmdvd-Types.html#OGMDvdAudioStream"/>
    <keyword type="struct" name="OGMDvdSubpStream" link="ogmdvd-Types.html#OGMDvdSubpStream"/>
    <keyword type="struct" name="OGMDvdReader" link="ogmdvd-Types.html#OGMDvdReader"/>
    <keyword type="struct" name="OGMDvdParser" link="ogmdvd-Types.html#OGMDvdParser"/>
    <keyword type="enum" name="enum OGMDvdVideoFormat" link="ogmdvd-Enumerations.html#OGMDvdVideoFormat"/>
    <keyword type="enum" name="enum OGMDvdDisplayAspect" link="ogmdvd-Enumerations.html#OGMDvdDisplayAspect"/>
    <keyword type="enum" name="enum OGMDvdDisplayFormat" link="ogmdvd-Enumerations.html#OGMDvdDisplayFormat"/>
    <keyword type="enum" name="enum OGMDvdAudioFormat" link="ogmdvd-Enumerations.html#OGMDvdAudioFormat"/>
    <keyword type="enum" name="enum OGMDvdAudioChannels" link="ogmdvd-Enumerations.html#OGMDvdAudioChannels"/>
    <keyword type="enum" name="enum OGMDvdAudioQuantization" link="ogmdvd-Enumerations.html#OGMDvdAudioQuantization"/>
    <keyword type="enum" name="enum OGMDvdAudioContent" link="ogmdvd-Enumerations.html#OGMDvdAudioContent"/>
    <keyword type="enum" name="enum OGMDvdSubpContent" link="ogmdvd-Enumerations.html#OGMDvdSubpContent"/>
    <keyword type="function" name="ogmdvd_get_video_format_label ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-video-format-label"/>
    <keyword type="function" name="ogmdvd_get_display_aspect_label ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-display-aspect-label"/>
    <keyword type="function" name="ogmdvd_get_audio_format_label ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-audio-format-label"/>
    <keyword type="function" name="ogmdvd_get_audio_channels_label ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-audio-channels-label"/>
    <keyword type="function" name="ogmdvd_get_audio_quantization_label ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-audio-quantization-label"/>
    <keyword type="function" name="ogmdvd_get_audio_content_label ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-audio-content-label"/>
    <keyword type="function" name="ogmdvd_get_subp_content_label ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-subp-content-label"/>
    <keyword type="function" name="ogmdvd_get_language_label ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-language-label"/>
    <keyword type="function" name="ogmdvd_get_language_iso639_1 ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-language-iso639-1"/>
    <keyword type="function" name="ogmdvd_get_language_iso639_2 ()" link="ogmdvd-Conversion-functions.html#ogmdvd-get-language-iso639-2"/>
    <keyword type="function" name="ogmdvd_reader_new ()" link="ogmdvd-Reader.html#ogmdvd-reader-new"/>
    <keyword type="function" name="ogmdvd_reader_new_by_cells ()" link="ogmdvd-Reader.html#ogmdvd-reader-new-by-cells"/>
    <keyword type="function" name="ogmdvd_reader_ref ()" link="ogmdvd-Reader.html#ogmdvd-reader-ref"/>
    <keyword type="function" name="ogmdvd_reader_unref ()" link="ogmdvd-Reader.html#ogmdvd-reader-unref"/>
    <keyword type="function" name="ogmdvd_reader_get_block ()" link="ogmdvd-Reader.html#ogmdvd-reader-get-block"/>
    <keyword type="function" name="ogmdvd_parser_new ()" link="ogmdvd-Parser.html#ogmdvd-parser-new"/>
    <keyword type="function" name="ogmdvd_parser_ref ()" link="ogmdvd-Parser.html#ogmdvd-parser-ref"/>
    <keyword type="function" name="ogmdvd_parser_unref ()" link="ogmdvd-Parser.html#ogmdvd-parser-unref"/>
    <keyword type="function" name="ogmdvd_parser_analyze ()" link="ogmdvd-Parser.html#ogmdvd-parser-analyze"/>
    <keyword type="function" name="ogmdvd_parser_get_max_frames ()" link="ogmdvd-Parser.html#ogmdvd-parser-get-max-frames"/>
    <keyword type="function" name="ogmdvd_parser_set_max_frames ()" link="ogmdvd-Parser.html#ogmdvd-parser-set-max-frames"/>
    <keyword type="function" name="ogmdvd_parser_get_audio_bitrate ()" link="ogmdvd-Parser.html#ogmdvd-parser-get-audio-bitrate"/>
    <keyword type="enum" name="enum OGMDvdParserStatus" link="ogmdvd-Parser.html#OGMDvdParserStatus"/>
    <keyword type="function" name="ogmdvd_disc_new ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-new"/>
    <keyword type="function" name="ogmdvd_disc_open ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-open"/>
    <keyword type="function" name="ogmdvd_disc_close ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-close"/>
    <keyword type="function" name="ogmdvd_disc_is_open ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-is-open"/>
    <keyword type="function" name="ogmdvd_disc_ref ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-ref"/>
    <keyword type="function" name="ogmdvd_disc_unref ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-unref"/>
    <keyword type="function" name="ogmdvd_disc_get_label ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-get-label"/>
    <keyword type="function" name="ogmdvd_disc_get_id ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-get-id"/>
    <keyword type="function" name="ogmdvd_disc_get_device ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-get-device"/>
    <keyword type="function" name="ogmdvd_disc_get_vmg_size ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-get-vmg-size"/>
    <keyword type="function" name="ogmdvd_disc_get_n_titles ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-get-n-titles"/>
    <keyword type="function" name="ogmdvd_disc_get_nth_title ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-get-nth-title"/>
    <keyword type="function" name="ogmdvd_disc_get_titles ()" link="ogmdvd-OGMDvdDisc.html#ogmdvd-disc-get-titles"/>
    <keyword type="enum" name="enum OGMDvdDiscError" link="ogmdvd-OGMDvdDisc.html#OGMDvdDiscError"/>
    <keyword type="function" name="ogmdvd_title_open ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-open"/>
    <keyword type="function" name="ogmdvd_title_close ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-close"/>
    <keyword type="function" name="ogmdvd_title_is_open ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-is-open"/>
    <keyword type="function" name="ogmdvd_title_ref ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-ref"/>
    <keyword type="function" name="ogmdvd_title_unref ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-unref"/>
    <keyword type="function" name="ogmdvd_title_analyze ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-analyze"/>
    <keyword type="function" name="ogmdvd_title_get_disc ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-disc"/>
    <keyword type="function" name="ogmdvd_title_get_vts_size ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-vts-size"/>
    <keyword type="function" name="ogmdvd_title_get_nr ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-nr"/>
    <keyword type="function" name="ogmdvd_title_get_ts_nr ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-ts-nr"/>
    <keyword type="function" name="ogmdvd_title_get_length ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-length"/>
    <keyword type="function" name="ogmdvd_title_get_chapters_length ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-chapters-length"/>
    <keyword type="function" name="ogmdvd_title_get_framerate ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-framerate"/>
    <keyword type="function" name="ogmdvd_title_get_size ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-size"/>
    <keyword type="function" name="ogmdvd_title_get_video_format ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-video-format"/>
    <keyword type="function" name="ogmdvd_title_get_display_aspect ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-display-aspect"/>
    <keyword type="function" name="ogmdvd_title_get_display_format ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-display-format"/>
    <keyword type="function" name="ogmdvd_title_get_palette ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-palette"/>
    <keyword type="function" name="ogmdvd_title_get_n_angles ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-n-angles"/>
    <keyword type="function" name="ogmdvd_title_get_n_chapters ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-n-chapters"/>
    <keyword type="function" name="ogmdvd_title_get_n_audio_streams ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-n-audio-streams"/>
    <keyword type="function" name="ogmdvd_title_get_nth_audio_stream ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-nth-audio-stream"/>
    <keyword type="function" name="ogmdvd_title_get_audio_streams ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-audio-streams"/>
    <keyword type="function" name="ogmdvd_title_get_n_subp_streams ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-n-subp-streams"/>
    <keyword type="function" name="ogmdvd_title_get_nth_subp_stream ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-nth-subp-stream"/>
    <keyword type="function" name="ogmdvd_title_get_subp_streams ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-subp-streams"/>
    <keyword type="function" name="ogmdvd_title_get_aspect_ratio ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-get-aspect-ratio"/>
    <keyword type="function" name="ogmdvd_title_find_audio_stream ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-find-audio-stream"/>
    <keyword type="function" name="ogmdvd_title_find_subp_stream ()" link="ogmdvd-OGMDvdTitle.html#ogmdvd-title-find-subp-stream"/>
    <keyword type="function" name="ogmdvd_stream_ref ()" link="ogmdvd-OGMDvdStream.html#ogmdvd-stream-ref"/>
    <keyword type="function" name="ogmdvd_stream_unref ()" link="ogmdvd-OGMDvdStream.html#ogmdvd-stream-unref"/>
    <keyword type="function" name="ogmdvd_stream_get_title ()" link="ogmdvd-OGMDvdStream.html#ogmdvd-stream-get-title"/>
    <keyword type="function" name="ogmdvd_stream_get_id ()" link="ogmdvd-OGMDvdStream.html#ogmdvd-stream-get-id"/>
    <keyword type="function" name="ogmdvd_stream_get_nr ()" link="ogmdvd-OGMDvdStream.html#ogmdvd-stream-get-nr"/>
    <keyword type="function" name="ogmdvd_audio_stream_get_format ()" link="ogmdvd-OGMDvdAudio.html#ogmdvd-audio-stream-get-format"/>
    <keyword type="function" name="ogmdvd_audio_stream_get_channels ()" link="ogmdvd-OGMDvdAudio.html#ogmdvd-audio-stream-get-channels"/>
    <keyword type="function" name="ogmdvd_audio_stream_get_language ()" link="ogmdvd-OGMDvdAudio.html#ogmdvd-audio-stream-get-language"/>
    <keyword type="function" name="ogmdvd_audio_stream_get_quantization ()" link="ogmdvd-OGMDvdAudio.html#ogmdvd-audio-stream-get-quantization"/>
    <keyword type="function" name="ogmdvd_audio_stream_get_content ()" link="ogmdvd-OGMDvdAudio.html#ogmdvd-audio-stream-get-content"/>
    <keyword type="function" name="ogmdvd_audio_stream_get_bitrate ()" link="ogmdvd-OGMDvdAudio.html#ogmdvd-audio-stream-get-bitrate"/>
    <keyword type="function" name="ogmdvd_subp_stream_get_language ()" link="ogmdvd-OGMDvdSubp.html#ogmdvd-subp-stream-get-language"/>
    <keyword type="function" name="ogmdvd_subp_stream_get_content ()" link="ogmdvd-OGMDvdSubp.html#ogmdvd-subp-stream-get-content"/>
    <keyword type="function" name="ogmdvd_monitor_get_default ()" link="ogmdvd-OGMDvdMonitor.html#ogmdvd-monitor-get-default"/>
    <keyword type="function" name="ogmdvd_monitor_get_drives ()" link="ogmdvd-OGMDvdMonitor.html#ogmdvd-monitor-get-drives"/>
    <keyword type="function" name="ogmdvd_monitor_get_drive ()" link="ogmdvd-OGMDvdMonitor.html#ogmdvd-monitor-get-drive"/>
    <keyword type="struct" name="struct OGMDvdMonitor" link="ogmdvd-OGMDvdMonitor.html#OGMDvdMonitor"/>
    <keyword type="function" name="ogmdvd_drive_get_device ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-get-device"/>
    <keyword type="function" name="ogmdvd_drive_get_gdrive ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-get-gdrive"/>
    <keyword type="function" name="ogmdvd_drive_get_name ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-get-name"/>
    <keyword type="function" name="ogmdvd_drive_get_drive_type ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-get-drive-type"/>
    <keyword type="function" name="ogmdvd_drive_get_medium_type ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-get-medium-type"/>
    <keyword type="function" name="ogmdvd_drive_load ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-load"/>
    <keyword type="function" name="ogmdvd_drive_can_eject ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-can-eject"/>
    <keyword type="function" name="ogmdvd_drive_eject ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-eject"/>
    <keyword type="function" name="ogmdvd_drive_is_door_open ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-is-door-open"/>
    <keyword type="function" name="ogmdvd_drive_lock ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-lock"/>
    <keyword type="function" name="ogmdvd_drive_unlock ()" link="ogmdvd-OGMDvdDrive.html#ogmdvd-drive-unlock"/>
    <keyword type="struct" name="struct OGMDvdDrive" link="ogmdvd-OGMDvdDrive.html#OGMDvdDrive"/>
  </functions>
</book>
