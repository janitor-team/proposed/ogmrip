<?xml version="1.0"?>
<!-- $Id: classes.xsl 183 2005-02-03 14:48:06Z billl $ -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <xsl:output method="html" encoding="UTF-8" doctype-public="-//W3C//DTD HTML 4.01//EN" doctype-system="http://www.w3.org/TR/html4/strict.dtd"/>

  <xsl:template match="html">
    <html>
      <head>
        <title>
          <xsl:value-of select="title"/>
          <xsl:text> - </xsl:text>
          <xsl:value-of select="subtitle"/>
        </title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></meta>
        <link rel="stylesheet" type="text/css" href="../data/style.css"/>
      </head>
      <body id="twocolumn-right">
        <div id="container">
          <div class="wrapper">
            <div id="header">
              <div class="wrapper">
                <h1 id="page-title">
                  <xsl:value-of select="title"/>
                </h1>
                <div style="clear: both"></div>
                <!-- <p class="description"> -->
                  <div id="g_description">
                    <table>
                      <tr>
                        <td style="margin: 0; padding: 0; width: 100%;font-size:1em;">
                          <xsl:value-of select="subtitle"/>
                        </td>
                        <td style="margin: 0; padding: 0; text-align: right;">
                          <xsl:apply-templates select="alternates/alternate"/>
                        </td>
                      </tr>
                    </table>
                  </div>
                  <!-- </p> -->
                <div style="clear: both"></div>
              </div>
              <!-- /wrapper -->
            </div>
            <!-- /header -->
            <div id="main-content">
              <div class="wrapper">
                <div class="content-item">
                  <div id="g_body">
                    <xsl:copy-of select="content/*"/>
                  </div>
                </div>
                <div style="clear: both"></div>
              </div>
              <!-- /wrapper -->
            </div>
            <!-- /main-content -->
            <div id="sidebar">
              <div class="wrapper">
                <div class="links">
                  <div class="wrapper">
                    <div id="g_sidebar">
                      <xsl:copy-of select="sidebar/*"/>
                    </div>
                  </div>
                  <!-- /wrapper -->
                  <div style="clear: both"></div>
                </div>
                <!-- /links -->
              </div>
              <!-- /wrapper -->
            </div>
            <!-- /sidebar -->
            <div id="footer">
              <div class="wrapper">
                <hr />
                <!-- <p> -->
                  <div id="g_footer">
                    <p style="text-align: center;">
                      <xsl:copy-of select="footer/*"/>
                    </p>
                  </div>
                  <!-- </p> -->
                <div style="clear: both"></div>
              </div>
              <!-- /wrapper -->
            </div>
            <!-- /footer -->
          </div>
          <!-- /wrapper -->
        </div>
        <!-- /container -->
        <div id="extraDiv1">
          <span></span>
        </div>
        <div id="extraDiv2">
          <span></span>
        </div>
        <div id="extraDiv3">
          <span></span>
        </div>
        <div id="extraDiv4">
          <span></span>
        </div>
        <div id="extraDiv5">
          <span></span>
        </div>
        <div id="extraDiv6">
          <span></span>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="alternate">
    <xsl:element name="a">
      <xsl:attribute name="href">
        <xsl:text>../</xsl:text>
        <xsl:value-of select="text()"/>
      </xsl:attribute>
      <xsl:value-of select="@name"/>
    </xsl:element>
  </xsl:template>
  
</xsl:stylesheet>

