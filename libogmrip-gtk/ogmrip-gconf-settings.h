/* OGMRip - A library for DVD ripping and encoding
 * Copyright (C) 2004-2012 Olivier Rolland <billl@users.sourceforge.net>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __OGMRIP_GCONF_SETTINGS_H__
#define __OGMRIP_GCONF_SETTINGS_H__

#include <ogmrip-settings.h>

G_BEGIN_DECLS

#define OGMRIP_TYPE_GCONF_SETTINGS            (ogmrip_gconf_settings_get_type ())
#define OGMRIP_GCONF_SETTINGS(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), OGMRIP_TYPE_GCONF_SETTINGS, OGMRipGConfSettings))
#define OGMRIP_GCONF_SETTINGS_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), OGMRIP_TYPE_GCONF_SETTINGS, OGMRipGConfSettingsClass))
#define OGMRIP_IS_GCONF_SETTINGS(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, OGMRIP_TYPE_GCONF_SETTINGS))
#define OGMRIP_IS_GCONF_SETTINGS_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), OGMRIP_TYPE_GCONF_SETTINGS))

typedef struct _OGMRipGConfSettings      OGMRipGConfSettings;
typedef struct _OGMRipGConfSettingsClass OGMRipGConfSettingsClass;
typedef struct _OGMRipGConfSettingsPriv  OGMRipGConfSettingsPriv;

struct _OGMRipGConfSettings
{
  GObject parent_instance;

  OGMRipGConfSettingsPriv *priv;
};

struct _OGMRipGConfSettingsClass
{
  GObjectClass parent_class;
};

GType            ogmrip_gconf_settings_get_type (void);
OGMRipSettings * ogmrip_gconf_settings_new      (const gchar *basedir);

G_END_DECLS

#endif /* __OGMRIP_GCONF_SETTINGS_H__ */

